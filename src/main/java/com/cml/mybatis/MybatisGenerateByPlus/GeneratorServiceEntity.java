package com.cml.mybatis.MybatisGenerateByPlus;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * <p>
 * 测试生成代码 http://baomidou.oschina.io/mybatis-plus-doc/#/generate-code
 * </p>
 *
 * @author K神
 * @date 2017/12/18
 */
public class GeneratorServiceEntity {

	public static void main(String[] args) {
		new GeneratorServiceEntity().generateCode();
	}

	public void generateCode() {
		String packageName = "com.baomidou.springboot";
		generateByTables(packageName, "sys_menu", "sys_user");
	}

	private void generateByTables(String packageName, String... tableNames) {

		String dbUrl = "jdbc:mysql://localhost:3306/guns";
		DataSourceConfig dataSourceConfig = new DataSourceConfig();

		dataSourceConfig.setDbType(DbType.MYSQL).setUrl(dbUrl).setUsername("root").setPassword("admin")
				.setDriverName("com.mysql.jdbc.Driver");
		dataSourceConfig.setTypeConvert(new MySqlTypeConvert() {
			@Override
			public DbColumnType processTypeConvert(String fieldType) {
				return super.processTypeConvert(fieldType);
			}
		});

		StrategyConfig strategyConfig = new StrategyConfig();
		strategyConfig.setCapitalMode(true).setEntityLombokModel(false).setDbColumnUnderline(true)
				.setNaming(NamingStrategy.underline_to_camel);// 修改替换成你需要的表名，多个表名传数组
		GlobalConfig config = new GlobalConfig();

		config.setMapperName("%sDao");
		config.setXmlName("%s-r");
		config.setServiceName("MP%sService");
		config.setServiceImplName("%sServiceDiy");
		config.setControllerName("%sAction");
		config.setEnableCache(false);
		config.setBaseResultMap(true);
		config.setBaseColumnList(true);
		config.setActiveRecord(false).setAuthor("cml").setOutputDir("d:\\codeGen").setFileOverride(true);

		new AutoGenerator().setGlobalConfig(config).setDataSource(dataSourceConfig).setStrategy(strategyConfig)
				.setTemplate(new TemplateConfig()).setPackageInfo(new PackageConfig().setParent(packageName)
						.setController("controller").setEntity("beans").setMapper("dao"))
				.execute();
	}
}
